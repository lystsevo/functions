const getSum = (str1, str2) => {

  if (typeof str1 !== "string" || typeof str2 !== "string") {
    return false;
  }
  if (str1 === "" || str2 === "") {
    return str1 + str2;
  }

  let re = /\D/;
  if (re.test(str1) || re.test(str2)) {
    return false;
  }

  let len = str1.length > str2.length ? str1.length : str2.length;

  let temp1 = "";
  let temp2 = "";

  for (let i = str1.length - 1; i >= 0; i--) {
    temp1 += str1[i];
  }

  for (let i = str2.length - 1; i >= 0; i--) {
    temp2 += str2[i];
  }

  let result = "";

  for (let i = 0; i < len; i++) {
    result += +temp1[i] + +temp2[i];
  }


  let res = "";
  for (let i = len - 1; i >= 0; i--) {
    res += result[i];
  }

  return res + "";
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let post = 0;
    let comments = 0;


    for (let item of listOfPosts) {
        if (item.author === authorName) {
            post++;
        }
        if (typeof item.comments === "object") {

            for (let com of item.comments) {
                if (com.author === authorName) {
                    comments++;
                }
            }
        }
    }

    return `Post:${post},comments:${comments}`
};


const tickets = (people) => {

  let kasa = [];

  let myFunc = (kas) => {
    let count = 0;
    for (let tisket of kas)
      if (tisket === 25)
        count++;
    return count;
  }

  for (let item of people) {

    if (item === 25) {
      kasa.push(item);
    } else if (item === 50) {
      if (kasa.includes(25)) {
        kasa.splice(kasa.indexOf(25), 1);
        kasa.push(item);
      } else {
        return 'NO'
      }
    } else if (item === 100) {
      if (kasa.includes(25) && kasa.includes(50)) {
        kasa.splice(kasa.indexOf(25), 1);
        kasa.splice(kasa.indexOf(50), 1);
        kasa.push(item);
      } else if (myFunc(kasa) > 2) {
        kasa.splice(kasa.indexOf(25), 1);
        kasa.splice(kasa.indexOf(25), 1);
        kasa.splice(kasa.indexOf(25), 1);
        kasa.push(item);
      } else {
        return 'NO'
      }
    }
  }

  return 'YES';
};


module.exports = {
  getSum,
  getQuantityPostsByAuthor,
  tickets
};